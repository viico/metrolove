var stationList_Empty = [
    {
        StationID: 7,
        StationSchedules: [
            {
                Way: "高桥西",
                Schedules: [
                    "08:50",
                    "10:50",
                    "12:50",
                    "14:50",
                ]
            },
            {
                Way: "霞浦",
                Schedules: [
                    "07:00",
                    "09:00",
                    "10:00",
                    "11:00",
                ]
            }
        ],
        Name: "高桥西"
    },
    {
        StationID: 0,
        StationSchedules: [
            {
                Way: "高桥西",
                Schedules: [
                    "08:40",
                    "10:40",
                    "12:40",
                    "14:40",
                ]
            },
            {
                Way: "霞浦",
                Schedules: [
                    "07:10",
                    "09:10",
                    "10:10",
                    "11:10",
                ]
            }
        ],
        Name: "测试1"
    },
    {
        StationID: 0,
        StationSchedules: [
            {
                Way: "高桥西",
                Schedules: [
                    "08:30",
                    "10:30",
                    "12:30",
                    "14:30",
                ]
            },
            {
                Way: "霞浦",
                Schedules: [
                    "07:20",
                    "09:20",
                    "10:20",
                    "11:20",
                ]
            }
        ],
        Name: "测试2"
    },
    {
        StationID: 0,
        StationSchedules: [
            {
                Way: "高桥西",
                Schedules: [
                    "08:20",
                    "10:20",
                    "12:20",
                    "14:20",
                ]
            },
            {
                Way: "霞浦",
                Schedules: [
                    "07:30",
                    "09:30",
                    "10:30",
                    "11:30",
                ]
            }
        ],
        Name: "测试3"
    },
    {
        StationID: 0,
        StationSchedules: [
            {
                Way: "高桥西",
                Schedules: [
                    "08:10",
                    "10:10",
                    "12:10",
                    "14:10",
                ]
            },
            {
                Way: "霞浦",
                Schedules: [
                    "07:40",
                    "09:40",
                    "10:40",
                    "11:40",
                ]
            }
        ],
        Name: "测试4"
    },
    {
        StationID: 83,
        StationSchedules: [
            {
                Way: "高桥西",
                Schedules: [
                    "08:00",
                    "10:00",
                    "12:00",
                    "14:00",
                ]
            },
            {
                Way: "霞浦",
                Schedules: [
                    "07:50",
                    "09:50",
                    "10:50",
                    "11:50",
                ]
            }
        ],
        Name: "霞浦"
    }
];
