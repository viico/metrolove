var stationList = stationList_Empty;
var Vue_SearchBox = new Vue({
    el: ".search_box",
    data: {
        StationListGetted: false,
        StationList: stationList,

        Sirection: 1,
        Station_Start: stationList_Empty[0],
        Station_End: stationList_Empty[stationList_Empty.length-1],

        Station_SelectedName: "",

        SchedulesList: [],
        SchedulesList_valid: 0,
    },
    created: function() {
        ajaxPecialTrainSchedules();
        // buildTimeList(stationList_Empty)
    },
    methods: {
        methods_exchangeDirection: function() {
            exchangeDirection();
        },
        methods_selectStation_show: function() {
            selectStation_show()
        },
        methods_selectStation_selected: function() {
            selectStation_selected(this.Station_SelectedName);
        },


        btn_touchStart: function(el,className) {
            $(el.currentTarget).addClass(className);
        },
        btn_touchEnd: function(el,className) {
            $(el.currentTarget).removeClass(className);
        }
    }
});
var Vue_TopBox = new Vue({
    el: ".top_box",
    data: {
        StationName_Start: "加载中……",
        StationName_End: "加载中……",
        Station_Start_Now: {
            StationID: 0,
            Name: "加载中……",
            Way: "",
            Time: ""
        },
        Station_End_Now: {
            StationID: 0,
            Name: "加载中……",
            Way: "",
            Time: ""
        },
    },
    methods: {

    }
});



function ajaxPecialTrainSchedules() {
    $.ajax({
        url: "https://app.ditiego.net/swms/slim/GetSpecialTrainSchedulesbyid", //请求的URL
        cache: false, //不从缓存中取数据
        data: {
            SpecialTraionID: 6
        }, //发送的参数
        type: 'POST', //请求类型
        dataType: 'jsonp', //返回类型是JSON
        timeout: 20000, //超时
        error: function (xhr, err) {
            alert(xhr.responseText);
        },
        success: function (data) //成功处理
        {
            stationList = data.Data.StationList;
            // buildTimeList(stationList);
            sirctionChanged(1);
            Vue_SearchBox.StationListGetted = true;
            metroSeat();
        }
    });
}

//切换方向
function exchangeDirection() {
    var sirction = Vue_SearchBox.Sirection;
    if(sirction == 1){
        Vue_SearchBox.Sirection = 2;
        sirctionChanged(2);
    }else if(sirction == 2){
        Vue_SearchBox.Sirection = 1;
        sirctionChanged(1);
    }
}
//正序
function sirctionChanged(sir) {
    var stationList_prepare = [];
    if(sir == 1){
        Vue_SearchBox.Station_Start = stationList[0];
        Vue_SearchBox.Station_End = stationList[stationList.length-1];
        for(var i=0; i<stationList.length; i++){
            stationList_prepare.push(stationList[i]);
        }
    }
    if(sir == 2){
        Vue_SearchBox.Station_Start = stationList[stationList.length-1];
        Vue_SearchBox.Station_End = stationList[0];
        for(var i=stationList.length-1; i>-1; i--){
            stationList_prepare.push(stationList[i]);
        }
    }
    Vue_SearchBox.StationList = stationList_prepare;
    Vue_SearchBox.Station_SelectedName = Vue_SearchBox.StationList[0].Name;
    Vue_SearchBox.methods_selectStation_selected();
}
//反序

//站点选择_开始
function selectStation_show() {
    
}
//站点选择_选择完成
function selectStation_selected(name) {
    var stationSelected = {};
    for(var i=0; i<stationList.length; i++){
        if(stationList[i].Name == name){
            stationSelected = stationList[i];
        }
    }
    var StationSchedule = [];
    var StationSchedules = stationSelected.StationSchedules;

    for(var i=0; i<StationSchedules.length; i++){
        if(StationSchedules[i].Way == Vue_SearchBox.Station_End.Name){
            StationSchedule = StationSchedules[i].Schedules;
        }
    }

    var date = new Date();
    var date_Minutes = parseInt(date.getHours())*60+parseInt(date.getMinutes());

    var schedulesList = [];
    var valid = StationSchedule.length;
    for(var i=0; i<StationSchedule.length; i++){
        var time = StationSchedule[i];
        var time_Minutes = parseInt(time.slice(0,2))*60+parseInt(time.slice(3));
        var miss = false;
        if (time_Minutes<date_Minutes){
            miss = true;
            valid --;
        }
        var item_schedule = {
            miss: miss,
            time: time
        };
        schedulesList.push(item_schedule);
    }
    Vue_SearchBox.SchedulesList = schedulesList;
    Vue_SearchBox.SchedulesList_valid = valid;
}

function metroSeat() {
    var gap_S = 100;
    var gap_E = -100;
    var station_S = {};
    var station_E = {};

    var date = new Date();
    var now = date.getHours()+":"+date.getMinutes();

    var way = ["加载中……","加载中……"];

    for (var a=0; a<Vue_SearchBox.StationList.length; a++){
        var stationSchedules = Vue_SearchBox.StationList[a].StationSchedules;
        for (var b=0; b<stationSchedules.length; b++){
            var schedules = stationSchedules[b].Schedules;
            for (var c=0; c<schedules.length; c++){
                var time = schedules[c];
                var gap = todayMinute(now)-todayMinute(time);
                if(gap>=0 && gap<gap_S){
                    gap_S = gap;
                    station_S = {
                        StationID: Vue_SearchBox.StationList[a].StationID,
                        Name: Vue_SearchBox.StationList[a].Name,
                        Way: stationSchedules[b].Way,
                        Time: time
                    };
                }
                if(gap<=0 && gap>gap_E){
                    if(gap_S == 0 && gap == 0){
                        continue;
                    }
                    gap_E = gap;
                    station_E = {
                        StationID: Vue_SearchBox.StationList[a].StationID,
                        Name: Vue_SearchBox.StationList[a].Name,
                        Way: stationSchedules[b].Way,
                        Time: time
                    };

                    way[0] = stationSchedules[0].Way;
                    way[1] = stationSchedules[1].Way;
                }
            }
        }
    }
    Vue_TopBox.Station_Start_Now = station_S;
    Vue_TopBox.Station_End_Now = station_E;
    Vue_TopBox.StationName_End = Vue_TopBox.Station_Start_Now.Way;
    if(Vue_TopBox.StationName_End == way[0]){
        Vue_TopBox.StationName_Start = way[1];
    }else {
        Vue_TopBox.StationName_Start = way[0];
    }
    console.log(station_S.Name+"到"+station_E.Name);
    setTimeout(function () {
        metroSeat()
    },15000);
}

function todayMinute(time) {
    if(time) {
        return parseInt(time.slice(0,2))*60+parseInt(time.slice(3));
    }else {
        var date = new Date();
        return parseInt(date.getHours())*60+parseInt(date.getMinutes());
    }
}

function copyObject(org) {
    // 1. copy has same prototype as org
    var copy = Object.create(Object.getPrototypeOf(org));

    // 2. copy has all of org's properties
    copyOwnPropertiesFrom(copy, org);

    return copy;
}

function copyOwnPropertiesFrom(target, source) {
    Object.getOwnPropertyNames(source)
        .forEach(function(propertyKey) {
            var desc = Object.getOwnPropertyDescriptor(source, propertyKey);
            Object.defineProperty(target, propertyKey, desc);
        });

    return target;
}